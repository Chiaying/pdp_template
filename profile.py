#!/usr/bin/python

"""
Four SDR nodes running GNU Radio with four RF links.
"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn

setup_command = "/local/repository/startup.sh"
installs = ["gnuradio"]

request = portal.context.makeRequestRSpec()

tx0 = request.RawPC( "tx0" )
tx0.hardware_type = "nuc5300"
tx0.disk_image = "urn:publicid:IDN+emulab.net+image+patawari:pdp_template.tx0"

service_command = " ".join([setup_command] + installs)
tx0.addService(rspec.Execute(shell="bash", command=service_command))

tx0if0 = tx0.addInterface( "rf0" )
# tx0if1 = tx0.addInterface( "rf1" )

# tx1 = request.RawPC( "tx1" )
# tx1.hardware_type = "nuc5300"
# tx1.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU18-64-GR38-PACK"
# service_command = " ".join([setup_command] + installs)
# tx1.addService(rspec.Execute(shell="bash", command=service_command))

# tx1if0 = tx1.addInterface( "rf0" )
# tx1if1 = tx1.addInterface( "rf1" )

rx0 = request.RawPC( "rx0" )
rx0.hardware_type = "nuc5300" #emulab-ops//UBUNTU18-64-STD
rx0.disk_image = "urn:publicid:IDN+emulab.net+image+patawari:pdp_template.rx0"
service_command = " ".join([setup_command] + installs)
rx0.addService(rspec.Execute(shell="bash", command=service_command))

rx0if0 = rx0.addInterface( "rf0" )
# rx0if1 = rx0.addInterface( "rf1" )

# rx1 = request.RawPC( "rx1" )
# rx1.hardware_type = "nuc5300"
# rx1.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU18-64-GR38-PACK"
# service_command = " ".join([setup_command] + installs)
# rx1.addService(rspec.Execute(shell="bash", command=service_command))

# rx1if0 = rx1.addInterface( "rf0" )
# rx1if1 = rx1.addInterface( "rf1" )

rflink00 = request.RFLink( "rflink00" )
rflink00.addInterface( tx0if0 )
rflink00.addInterface( rx0if0 )

# rflink01 = request.RFLink( "rflink01" )
# rflink01.addInterface( tx0if1 )
# rflink01.addInterface( rx1if0 )

# rflink10 = request.RFLink( "rflink10" )
# rflink10.addInterface( tx1if0 )
# rflink10.addInterface( rx0if1 )

# rflink11 = request.RFLink( "rflink11" )
# rflink11.addInterface( tx1if1 )
# rflink11.addInterface( rx1if1 )

portal.context.printRequestRSpec()
